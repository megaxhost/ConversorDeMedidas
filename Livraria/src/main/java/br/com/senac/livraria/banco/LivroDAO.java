package br.com.senac.livraria.banco;

import br.com.senac.livraria.entity.Livro;

public class LivroDAO extends DAO<Livro> {

    public LivroDAO() {
        super(Livro.class);
    }

}
