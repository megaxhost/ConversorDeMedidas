/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.entity;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Administrador
 */
public class TesJPALista {
    
    public static void main(String... arg) {
        
        Genero g1 = new Genero();
        g1.setDescricao("Drama1");
        
        Genero g2 = new Genero();
        g2.setDescricao("Drama2");
        
        Genero g3 = new Genero();
        g3.setDescricao("Drama3");
        
        Genero g4 = new Genero();
        g4.setDescricao("Drama4");
        
        Genero g5 = new Genero();
        g5.setDescricao("Drama5");
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("LivrariaPU");
        
       EntityManager em = emf.createEntityManager ();
       
       em.getTransaction().begin();
       
       em.persist(g1);
       em.persist(g2);
       em.persist(g3);
       em.persist(g4);
       em.persist(g5);
       
       em.getTransaction().commit();
       
       List<Genero> lista = null;
       
       em.getTransaction().begin();
       
        Query query = em.createQuery("From Genero");
        
        lista = query.getResultList();
        
        em.getTransaction().commit();
        
        for (Genero g : lista) {
           System.out.println("Codigo:" + g.getId() + "Descricao" + g.getDescricao());  
            
        }
       
       
        
    }
    
}
