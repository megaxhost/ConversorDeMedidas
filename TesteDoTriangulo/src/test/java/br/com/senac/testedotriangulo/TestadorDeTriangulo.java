package br.com.senac.testedotriangulo;

import org.junit.Assert;
import org.junit.Test;

public class TestadorDeTriangulo {

    @Test
    public void testEhUmTriangulo() {
        Triangulo triangulo = new Triangulo(10, 10, 10);
        Assert.assertTrue(triangulo.isTriangulo());
    }

    @Test
    public void testNaoEhTriangulo() {
        Triangulo triangulo = new Triangulo(10, 100, 1);
        Assert.assertFalse(triangulo.isTriangulo());
    }

}
